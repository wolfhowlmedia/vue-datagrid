/**
 * @author Primoz Anzur
 * @package VUE-Datagrid
 * @copyright 2018- Primoz Anzur
 * @version 0.2-dev
 *
 * VueJS datagrid component:
 *
 * Data retrieval:
 *  - headers - Specify the fields and their attributes
 *    - field - field to be retrieved (string)
 *    - size  - minimal width of the field in pixels (integer)
 *    - style - plain CSS (string)
 *  - Headers also support short hands for clickable icons with callbacks:
 *    - view
 *    - edit
 *    - delete
 *    
 *  
 *  - filters - Specify the fields for which to enable filter fields
 *    - field name (string) or
 *    - object, consisting of field and options from which to choose from
 *  
 *  - source : Getting JSON data
 *    - url:       location of the source
 *    - method:    method of getting the data
 *    - grid_data: how to handle the source
 *      - data: Data container
 *      - row_ counter: How to count the rows (use 'internal' for using the internal calculator, anything else for JSON object)
 *      - filter: how to filter data (use 'internal' for internal field filtering)
 *    - itemsPerPage: How many entries there are per page
 *
 * @example
 *  <script type="text/javascript">
 *  document.addEventListener('DOMContentLoaded', function() {
 *      new Vue({
 *        el: '#app',
 *        data: {
 *         config: {
 *           headers: [
 *             {field: 'id', size: 20},
 *             {field: 'first_name', size: 150},
 *             {field: 'last_name',  size: 150},
 *             {field: 'email',      size: 150},
 *             {field: 'gender',     size: 150},
 *             {field: 'ip_address', size: 150},
 *             'edit',
 *             'view'
 *           ],
 *           filters: ['id', 'first_name', 'last_name', 'email', {'field':'gender', 'options':['male', 'female']}, 'ip_address'],
 *           source: {
 *             url: 'data/index.php',
 *             method: 'get',
 *             grid_data: {'data': 'data', 'row_counter': 'internal', 'filter':'internal'},
 *             itemsPerPage: 20
 *           }
 *        },
 *        methods: {
 *        }
 *      }});
 *  });
 *  </script>
 *
 *  <div id="app" class="container-fluid">
 *   <datagrid :config="config"></datagrid>
 *  </div>
 */

Vue.prototype.$http = window.axios;
Vue.component('datagrid', {
    template: [
        '<div class="table-responsive" style="position:relative;">',
          '<div style="position:relative;">',
            '<div v-if="config.show_settings" style="position:absolute; top:0; right:0; cursor:pointer;" @click="dlg_show_settings">',
              '<i class="fa fa-cog"></i>',
            '</div>',
            '<table :class="config.table_class">',
              '<thead>',
                '<tr>',
                  '<th v-if="config.rows_selectable" style="width:50px;" class="text-center">Select</th>',
                  '<th v-for="column in config.headers" :style="column[\'style\']" :class="{\'text-center\':column.special}">',
                    '<div @click="sort_by(column[\'field\'])"  style="cursor:pointer;width:100%" v-if="column[\'sortable\']">{{ column[\'title\'] | capitalize }} <i :class="{\'fa fa-sort-asc sort-asc\': sorted[column[\'field\']] == 1, \'fa fa-sort-desc sort-desc\': sorted[column[\'field\']] == 2 }"></i></div>',
                    '<div v-if="!column[\'sortable\']">{{ column[\'title\'] | capitalize }}</div>',
                  '</th>',
                '</tr>',
                '<tr v-if="has_filtered_fields()">',
                  '<td v-if="config.rows_selectable" class="text-center"><input type="checkbox" v-model="mark_all" @click="select_all" /></td>',
                  '<td v-for="column in config.headers" style="margin:5px;">',
                    '<input v-if="column.filtered && !column.options" type="text" :placeholder="\'Filter by \' + column.title" class="form-control input-sm" v-model="filter[column.field]" @change="filter_results()" />',
                    '<select v-else-if="column.filtered && column.options" class="form-control input-sm" v-model="filter[column.field]" @change="filter_results()">',
                      '<option value=""></option>',
                      '<option v-for="opt in column.options" v-bind:value="opt">{{ opt }}</option>',
                    '</select>',
                  '</td>',
                '</tr>',
              '</thead>',
              '<tbody v-if="results.total">',
                '<tr v-for="row in rows" :class="row.class">',
                  '<td v-if="config.rows_selectable" class="text-center"><input type="checkbox" v-model="selected_rows" :value="row" /></td>',
                  '<td v-for="column in config.headers" :class="{\'text-center\':column.special}">',
                    '<span v-if="column.special"><i :class="column[\'icon\']" @click="icon_click(column[\'callback\'], row)" style="cursor:pointer;"></i></span>',
                    '<span v-else-if="column.type">',
						'<div class="progress">',
							'<div class="progress-bar" \
								:class="{\'progress-bar-danger\': row[column[\'field\']] <= 10, \'progress-bar-warning\': row[column[\'field\']] > 10 && row[column[\'field\']] <= 50, \'progress-bar-info\': row[column[\'field\']] > 50 && row[column[\'field\']] <= 90, \'progress-bar-success\': row[column[\'field\']] > 90}" \
								role="progressbar" :aria-valuenow="row[column[\'field\']]" aria-valuemin="0" aria-valuemax="100" :style="{width: row[column[\'field\']] + \'%\'}">{{ row[column[\'field\']] }}%</div>',
						'</div>',
                    '</span>',
                    '<span v-else>{{ row[column[\'field\']] }}</span>',
                  '</td>',
                '</tr>',
              '</tbody>',
              '<tbody v-else>',
                '<tr>',
                	'<td :colspan="config.headers.length + 1">{{ config.empty_message }}</td>',
                '</tr>',
              '</tbody>',
            '</table>',
            '<div class="clearfix" v-if="results.total">',
              '<div class="pull-left">',
                'Showing results {{ results.from }} - {{ results.to }} of {{ results.total }}',
              '</div>',
              '<nav class="pull-right clearfix">',
                '<ul class="pagination pagination-sm" style="margin:0;">',
                  '<li v-if="current_page > 1">',
                    '<a href="#" aria-label="First" @click="nav_first">',
                      '<span aria-hidden="true" >&laquo;</span>',
                    '</a>',
                  '</li>',
                  '<li v-if="current_page > 1">',
                    '<a href="#" aria-label="Previous" @click="nav_previous">',
                      '<span aria-hidden="true" >&lsaquo;</span>',
                    '</a>',
                  '</li>',
                  '<li v-for="p in pages" :class="{active: is_current(p)}" @click="current_page = p; reload_data()" v-if="nav_show_page(p)"><a href="#" >{{ p }}</a></li>',
                  '<li v-if="current_page < pages">',
                    '<a href="#" aria-label="Next" @click="nav_next">',
                      '<span aria-hidden="true" >&rsaquo;</span>',
                    '</a>',
                  '</li>',
                  '<li v-if="current_page < pages">',
                    '<a href="#" aria-label="Last" @click="nav_last">',
                      '<span aria-hidden="true" >&raquo;</span>',
                    '</a>',
                  '</li>',
                '</ul>',
              '</nav>',
            '</div>',
          '</div>',
        '</div>'
        ].join(''),
    data: function() { 
      var sorted = {};

      //Sort by field
      this.config.headers.forEach(function(obj){
        sorted[obj['field']] = 0;
      });

      return {
        mark_all: false,
        selected_rows: [],
        pages:1,
        current_page:1,
        filter:{},
        rows:[],
        results: {
          from: 0,
          to: 0,
          total: 0
        },
        sorted: sorted,
        header_fields:[],
        special_filters: []
      }
    },
    props: {
      config: {
        rows_selectable: {default: false},
        headers: {default : []}, 
        filters: {default : []},
        additional_fields : {default: []},
        source: {default  : Object},
        empty_message: "No matches found."
      }
    },
    methods: {
      dlg_show_settings: function() {
        console.log("Settings had been clicked.");
      },
      filtered_field: function (column) {
        return true;
      },
      has_filtered_fields: function() {
        for (var x = 0; x < this.config.headers.length; x++) {
          if(this.config.headers[x].filtered) {
            return true;
          }
        }
        return false;
      },
      is_current: function(index) {
        return index == this.current_page;
      },
      nav_first: function() {
        this.current_page = 1;
      }, 
      nav_last: function() {
        this.current_page = this.pages;
      }, 
      nav_next: function() {
        if(this.current_page < this.pages) {
          this.current_page = this.current_page + 1;
          this.reload_data();
        }
      }, 
      nav_previous: function() {
        if(this.current_page > 1) {
          this.current_page = this.current_page - 1;
          this.reload_data();
        }
      },
      nav_show_page: function(page) {
        if(page == 1 || page == this.pages || page == this.current_page || page == this.current_page - 1 || page == this.current_page + 1) {
          return true;
        }
      },
      filter_results: function() {
        this.current_page = 1;
        this.reload_data();
      },
      select_all: function() {
        this.selected_rows = [];
        if (!this.mark_all) {
          this.rows.forEach(function(elem){
            this.selected_rows.push(elem);
          }.bind(this));
        }
      },
      reload_data: function() {
        var fields = this.header_fields;
        
        if (typeof this.config.source.grid_data.data == 'undefined') {
          throw "options.grid_data.data is not defined! Grid cannot be rendered!";
        }

        if (typeof this.config.source.url == 'undefined') {
          throw "options.url is not defined! Grid cannot be rendered!";
        }

        if (typeof this.config.source.itemsPerPage == 'undefined') {
          this.config.source.itemsPerPage = 20;
        }

        if (typeof this.config.source.method == 'undefined') {
          this.config.source.method = 'post';
        }

        //@TODO: Filter magic
        var f = new Object();
        var cnt = 0;

        var calc_offset = this.config.source.itemsPerPage * (this.current_page - 1);
        var data = {paging: {limit: this.config.source.itemsPerPage, offset: calc_offset}};
        if(cnt > 0) {
          data['filter'] = f;
        }

        if (typeof this.config.source.grid_data.filter != 'undefined' && this.config.source.grid_data.filter == 'internal') {
          console.warn('Internal filter uses a lot of resorces, so you should preferably use something external!')
        }

        var o = [];
        for(var obj in this.sorted) {
          switch(this.sorted[obj]) {
            case 1:
              o.push({field:obj, direction:'asc'});
            break;
            case 2:
              o.push({field:obj, direction:'desc'});
            break;
          }
        }

        if (o.length > 0) {
          data['order'] = o;
        }

        var special_filters = [];
        for (var x = 0; x < this.config.headers.length; x++) {
          if (typeof this.config.headers[x].options == typeof [] || this.config.headers[x].autodetect) {
            special_filters.push(this.config.headers[x].field);
          }
          if (this.config.headers[x].autodetect) {
            this.config.headers[x].options = [];
          }
        }

        var filter_fields = this.filter;

        //Retrieve data from external source via Axios
        this.$http[this.config.source.method](this.config.source.url, {filter: JSON.stringify(data)}).then((response) => {
          var items = response.data[this.config.source.grid_data.data];
          var items_length = items.length;
          this.rows = [];

          //Traverse throughout the rows
          var filtered_cnt = 0;
          for(x = 0; x < items_length; x++) {
            var obj = {};
            var filtered_match = true;
            for(y = 0; y < fields.length; y++) {
              if(special_filters.includes(fields[y])) {
                if (this.config.headers[y].autodetect){ //Check if the option should be autodetected or not...
                  if (!this.config.headers[y].options) {
                    this.config.headers[y].options = []
                  }
                  if (!this.config.headers[y].options.includes(items[x][fields[y]])) {
                    this.config.headers[y].options.push(items[x][fields[y]])
                  }
                }
              }

              if (typeof items[x][fields[y]] != 'undefined' && typeof this.config.headers[y]['filter'] != 'undefined') {
                if (typeof this.$parent[this.config.headers[y]['filter']] != 'undefined') {
                  obj[fields[y]] = this.$parent[this.config.headers[y]['filter']](items[x][fields[y]]); //row filter callback
                } else {
                  console.log('A filter callback for row "' + fields[y] + '" is not defined!');
                }
              } else if (typeof items[x][fields[y]] != 'undefined') {
                obj[fields[y]] = items[x][fields[y]];
              } else if (typeof fields[y] == typeof {}) { //The item is a 'special' item (e.g.: "quick buttons")
                fields[y]['special'] = true;
                obj[fields[y]['name']] = fields[y];
              } else {
                console.log("I don't know what to do with " + fields[y]);
              }
            }

            var k = Object.keys(filter_fields);
            for(var i = 0; i < k.length; i++) {
              var re_pfx = '';

              if (special_filters.includes(k[i])) {
                re_pfx = '^';
              }
              var re = new RegExp(re_pfx + filter_fields[k[i]], 'i');
              var out = re.test(obj[k[i]]);
              if (!out) {
                filtered_match = false;
              }
            }

            if (filtered_match) {
              this.rows.push(obj);
              filtered_cnt ++;
            }
          }

          var cnt = 0;
          if (this.config.source.grid_data.row_counter == 'internal') {
            var cnt = this.rows.length;
          } else {
            var cnt = response.data[this.config.source.grid_data.row_counter];
          }
          var ipp = this.config.source.itemsPerPage;

          this.results['total'] = cnt;
          this.pages = Math.ceil(cnt / ipp);

          this.results['from'] = (ipp * (this.current_page - 1) + 1);
          this.results['to']   = (ipp * this.current_page - (ipp - cnt));

          if (!this.results['total']) {
            this.results['from'] = 0;
          }

        }, (response) => {
          this.rows = [];
          console.error('Unable to retrieve data. Server returned an error ' + response.status + ': "' + response.statusText + '" ');
        });
      },
      sort_by:function(field) {
        this.sorted[field] += 1;
        if (this.sorted[field] > 2) {
          this.sorted[field] = 0;
        }

        this.current_page = 1;
        this.reload_data();
      },
      icon_click: function(callback, row) {
        if (typeof this.$parent[callback] == 'undefined') {
          console.warn('Method ' + callback + ' is not defined!');
        } else {
          this.$parent[callback](row);
        }
      },
      build_headers: function() {
        //Preventing the crash if not defined
        if (typeof this.config.filters == 'undefined') {
          this.config.filters = [];
        }

        //build headers
        fields = [];
        cols   = [];
        this.config.headers.forEach(function(obj){
          if(typeof obj == 'string') {
            switch(obj) {
              case 'view':
                cols.push({title:'View', callback: 'cb_view', icon:'fa fa-eye', style:'width: 70px', 'special': true});
              break;
              case 'edit':
                cols.push({title:'Edit', callback: 'cb_edit', icon:'fa fa-edit', style:'width: 70px', 'special': true});
              break;
              case 'delete':
                cols.push({title:'Delete', callback: 'cb_delete', icon:'fa fa-trash', style:'width: 70px', 'special': true});
              break;
              default:
                console.log("The shorthand '" + obj +  "' is not defined. Please set it up manually.");
            }
          } else {
            if (typeof obj['title'] == 'undefined') {
              obj.title = obj['field'].replace('_', ' ');
            }

            if (typeof obj['size'] != 'undefined') {
              obj.style = 'width: ' + obj['size'].toString() + 'px';
            }

            if (typeof obj['sortable'] == 'undefined') {
              obj.sortable = true;
            }

            cols.push(obj);
            fields.push(obj['field']);
          }
          
        }.bind(this));

        this.header_fields  = fields;
        this.config.headers = cols;
      }
    },
    filters: {
      capitalize: function (value) {
        if (!value && value !== 0) return '';
        value = value.toString();
        return value.charAt(0).toUpperCase() + value.slice(1);
      },
    },
    created: function(){
      //initializations
      if  (typeof this.config.table_class == 'undefined' || typeof this.config.table_class != typeof []) {
        this.config.table_class = ['table','table-striped','table-condensed','table-hover','table-bordered'];
      }

      if (!this.config.table_class.includes['table']) {
        this.config.table_class.push('table');
      }

      if (!this.config.empty_message) {
      	this.config.empty_message = "No rows found!";
      }
    },
    watch: {
      selected_rows: function(oldVal, newVal) {
        out = [];
        this.rows.forEach(function(elem){
          elem.class = "";
          oldVal.find(function(e) {
            if (elem == e) {
              elem.class = "selected";
            }
          })
          out.push(elem);
        }.bind(this));

        this.rows = out;

        this.mark_all = (this.selected_rows.length == this.rows.length);
      }
    },
    mounted: function() {
      this.build_headers();

      //Load inital data
      this.reload_data();
    }
})
