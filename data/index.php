<?php

$out = [];

$rows = file('MOCK_DATA.csv');
foreach ($rows as $row_id => $row) {
	if (!$row_id) {
		continue;
	}

	//id,first_name,last_name,email,gender,ip_address
	$fields = explode(',', trim($row));
	$out[] = [
		'id'         => $fields[0],
		'first_name' => $fields[1],
		'last_name'  => $fields[2],
		'email'      => $fields[3],
		'gender'     => $fields[4],
		'ip_address' => $fields[5],
		'progress'   => rand(0,100)
	];
}

echo json_encode(['data' => $out]);
echo "\n";