<!DOCTYPE html><html lang="en">
<head>
	<title>Vue Datagrid Example</title>
	<link href="https://stackpath.bootstrapcdn.com/bootswatch/3.3.7/lumen/bootstrap.min.css" rel="stylesheet" crossorigin="anonymous">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.18.0/axios.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.5.17/vue.js" integrity="sha256-3kjowa0CUPPK4W+yaGpNP1exjs8gbX2SSsToEMd06K8=" crossorigin="anonymous"></script>
	<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
	
	<!-- component stuff -->
	<link href="components/css/datagrid.css" rel="stylesheet">
	<script type="text/javascript" src="components/datagrid.js"></script>

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/> 
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<script type="text/javascript">
	document.addEventListener('DOMContentLoaded', function() {
	    vm = new Vue({
	      el: '#app',
	      data: {
	       config: {
	       	rows_selectable: true,
	       	show_settings: true,
	        headers: [
	          {field: 'id', size: 20, filtered: true},
	          {field: 'first_name', size: 150, filtered: true},
	          {field: 'last_name',  size: 150, filtered: true},
	          {field: 'email',      size: 150, filtered: true},
	          {field: 'gender',     size: 150, filtered: true, autodetect: true},
	          {field: 'ip_address', size: 150, filtered: true},
	          {field: 'progress',   size: 150, filtered: false, type: 'progress'},
	          'edit',
	          'view'
	        ],
	        source: {
	          url: 'data/index.php',
	          method: 'get',
	          grid_data: {'data': 'data', 'row_counter': 'internal', 'filter':'internal'},
	          itemsPerPage: 20
	        },
	        table_class: ['table','table-striped','table-condensed','table-hover','table-bordered', 'table-fixed']
	      },
	    }});
	});
	</script>
</head>
<body>
	<div class="navbar navbar-default ">
		<div class="navbar-collapse collapse navbar-responsive-collapse">
			<a class="navbar-brand" href="#"><strong>VUE-DATAGRID EXAMPLE</strong></a>
		</div>
	</div>

	<div id="app" class="container-fluid">
		<datagrid :config="config"></datagrid>
	</div>
</body>
</html>

